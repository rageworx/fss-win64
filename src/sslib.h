#ifndef __SCREENSAVERLIB_H__
#define __SCREENSAVERLIB_H__

#define DLG_SCRNSAVECONFIGURE   2003
#define ID_APP                  100
#define IDS_DESCRIPTION         1

LRESULT WINAPI DefScreenSaverProc(HWND hwnd, UINT iMsg, WPARAM wparam, LPARAM lparam);
BOOL    WINAPI RegisterDialogClasses(HANDLE hinstance);
BOOL    WINAPI ScreenSaverConfigureDialog(HWND hwnd, UINT iMsg, WPARAM wparam, LPARAM lparam);
LRESULT WINAPI ScreenSaverProc(HWND hwnd, UINT iMsg, WPARAM wparam, LPARAM lparam);

#endif /// of __SCREENSAVERLIB_H__