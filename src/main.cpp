#include <windows.h>
#include "sslib.h"
#include "resource.h"

static PAINTSTRUCT  ps      = {NULL};
static HDC          hDC     = NULL;
static HBRUSH       hBrush  = NULL;
static UINT         uTimer  = 0;
static RECT         rc      = {0};
static wchar_t      title[] = L"F.S.S-Win64";

LRESULT WINAPI ScreenSaverProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	switch(message)
	{
        case WM_CREATE:
            hBrush = CreateSolidBrush(RGB(0, 0, 0));
            uTimer = SetTimer( hWnd, 1, 1000, NULL );
            break;

        case WM_DESTROY:
            if( uTimer > 0 )
                KillTimer( hWnd, uTimer );

            if( hBrush != NULL )
                DeleteObject(hBrush);

            PostQuitMessage(0);
            break;

        case WM_TIMER:
            SendMessage( hWnd, WM_CLOSE, 0, 0 );
            break;

        case WM_PAINT:
            hDC = BeginPaint(hWnd, &ps);

            SetBkColor(hDC, RGB(0, 0, 0));
            SetTextColor(hDC, RGB(255, 255, 0));
            TextOut(hDC, 0, 0, title, wcslen( title ) );
            
            EndPaint(hWnd, &ps);
            break;


        default: 
            return DefScreenSaverProc(hWnd, message, wParam, lParam);
	}
	
	return 0;
}

BOOL VerifyPassword(HWND hwnd) /*Thanks to B. G. Edmond for writing these to password functions.*/
{   
    return TRUE;
}

void ChangePassword(HWND hwnd)
{
    return;
}

BOOL WINAPI ScreenSaverConfigureDialog(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	return FALSE;
}

BOOL WINAPI RegisterDialogClasses(HANDLE hInst)
{
	return TRUE;
}