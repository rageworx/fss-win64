#include <windows.h>
#include <wchar.h>
#include "sslib.h"

#define WAKE_THRESHOLD 4

typedef VOID (WINAPI *PWDCHANGEPASSWORD)(LPCSTR lpcRegkeyname, HWND hwnd, UINT uiReserved1, UINT uiReserved2);
typedef BOOL (WINAPI *VERIFYSCREENSAVEPWD)(HWND hwnd);

enum 
{
    smNone, 
    smConfig, 
    smPassword, 
    smPreview, 
    smSaver
} g_scrmode;

static BOOL         g_fCheckingPassword = FALSE;
static BOOL         g_fClosing = FALSE;
static BOOL         g_fOnWin95 = FALSE;
static wchar_t      g_szClassName [] = L"WindowsScreenSaverClass";
static HINSTANCE    g_hinstPwdDLL;
static HINSTANCE    g_hinstance;
static POINT        g_ptMouse;
static UINT         uShellMessage;

static VERIFYSCREENSAVEPWD g_verifyScreenSavePwd;

inline unsigned long long wcs2ul( const wchar_t* x )
{
    unsigned long long ulx = 0;
    
    if ( x != NULL )
    {
        ulx = wcstoull( x, NULL, 0 );
    }
    
    return ulx;
}

// local function prototypes --
void doChangePwd(HWND hwnd);
void doConfig(HWND hwnd);
void doSaver(HWND hwndParent);
void hogMachine(BOOL fDisable);
void loadPwdDLL();
void unloadPwdDLL();

LRESULT CALLBACK RealScreenSaverProc (HWND hwnd, int iMsg, WPARAM wparam, LPARAM lparam);


#pragma argsused
int WINAPI WinMain( HINSTANCE hinstance, HINSTANCE hinstancePrev, LPSTR lpszCmdLine, int iCmdShow )
{
    wchar_t*        pch = NULL;
    HWND            hwnd;
    OSVERSIONINFO   osvi = {0};

    osvi.dwOSVersionInfoSize = sizeof(OSVERSIONINFO);

    if (GetVersionEx (&osvi) || osvi.dwPlatformId == VER_PLATFORM_WIN32_WINDOWS)
        g_fOnWin95 = TRUE;

    g_hinstance = hinstance;

    pch = GetCommandLine();

    if ( *pch == L'\"' )
    {
        pch++;

        while ( *pch != L'\0' && *pch != L'\"' )
            pch++;
        
    }
    else
    {
        while ( *pch != L'\0' && *pch != L' ' )
        pch++;
    }

    if ( *pch != L'\0' )
        pch++;

    while ( *pch == L' ' )
       pch++;

    if ( *pch == L'\0' )
    {
        g_scrmode = smConfig;
        hwnd = GetForegroundWindow ();
    }
    else
    {
        if ( *pch == L'-' || *pch == L'/' )
            pch++;

        if ( *pch == L'a' || *pch == L'A' )
        {
            g_scrmode = smPassword;

            pch++;

            while ( *pch == L' ' || *pch == L':' )
                pch++;

            hwnd = (HWND)wcs2ul(pch);
        }
        else
        if ( *pch == L'c' || *pch == L'C' )
        {
            g_scrmode = smConfig;

            pch++;

            while ( *pch == L' ' || *pch == L':' )
                pch++;

            if ( *pch == L'\0' )
                hwnd = GetForegroundWindow();
            else
                hwnd = (HWND)wcs2ul(pch);
        }
        else
        if ( *pch == L'p' || *pch == L'P' || *pch == L'l' || *pch == L'L' )
        {
            g_scrmode = smPreview;

            pch++;

            while ( *pch == L' ' || *pch == L':' )
                pch++;

            hwnd = (HWND)wcs2ul(pch);
        }
        else
        if ( *pch == L's' || *pch == L'S' )
        {
            g_scrmode = smSaver;
        }
    }

    if ( g_scrmode == smConfig )
        doConfig(hwnd);
    else
    if ( g_scrmode == smPassword )
        doChangePwd(hwnd);
    else
    if (g_scrmode == smPreview)
        doSaver(hwnd);
    else
    if (g_scrmode == smSaver)
        doSaver(NULL);

    return 0;
}

LRESULT WINAPI DefScreenSaverProc( HWND hwnd, UINT iMsg, WPARAM wparam, LPARAM lparam )
{
    POINT ptCheck;
    int deltax, deltay;

    if ( iMsg == uShellMessage )
    {
        if (uShellMessage == 0)
            return DefWindowProc (hwnd, iMsg, wparam, lparam);

        PostMessage (hwnd, WM_CLOSE, 0, 0);

        return (g_verifyScreenSavePwd) ? 1 : 0;
    }

    if ( g_scrmode != smPreview && !g_fClosing )
    {
        switch ( iMsg )
        {
            case WM_ACTIVATEAPP:
                if (wparam == FALSE)
                    PostMessage (hwnd, WM_CLOSE, 0, 0);
                break;

            case WM_CLOSE:
                if (!g_fOnWin95)
                  break;
              
                if (!g_verifyScreenSavePwd)
                {
                    g_fClosing = TRUE;
                    break;
                }


                g_fCheckingPassword = TRUE;
                g_fClosing = g_verifyScreenSavePwd (hwnd);
                g_fCheckingPassword = FALSE;

                if (g_fClosing)
                    break;

                GetCursorPos (&g_ptMouse);
                return 0;

            case WM_KEYDOWN:
            case WM_LBUTTONDOWN:
            case WM_MBUTTONDOWN:
            case WM_POWERBROADCAST:
            case WM_RBUTTONDOWN:
            case WM_SYSKEYDOWN:
                PostMessage (hwnd, WM_CLOSE, 0, 0);
                break;

            case WM_MOUSEMOVE:
                GetCursorPos (&ptCheck);

                deltax = ptCheck.x-g_ptMouse.x;
                if (deltax < 0)
                    deltax = -deltax;

                deltay = ptCheck.y-g_ptMouse.y;
                if (deltay < 0)
                    deltay = -deltay;

                deltay += deltax;

                if (deltay > WAKE_THRESHOLD)
                {
                    g_ptMouse.x = ptCheck.x;
                    g_ptMouse.y = ptCheck.y;
                    PostMessage (hwnd, WM_CLOSE, 0, 0);
                }
                break;

            case WM_POWER:
                if (wparam == PWR_CRITICALRESUME)
                  PostMessage (hwnd, WM_CLOSE, 0, 0);
                break;

            case WM_SETCURSOR:
                SetCursor (g_fCheckingPassword ? LoadCursor (NULL, IDC_ARROW) : NULL);
                return -1;
        }
    }

    return DefWindowProc (hwnd, iMsg, wparam, lparam);
}

void doChangePwd( HWND hwnd )
{
    HINSTANCE hinstMPRDLL;
    PWDCHANGEPASSWORD pwdChangePassword;

    if (!g_fOnWin95)
        return;

    hinstMPRDLL = LoadLibrary ( L"mpr.dll" );
    
    if (hinstMPRDLL == NULL)
        return;

    pwdChangePassword = (PWDCHANGEPASSWORD)GetProcAddress( hinstMPRDLL, "PwdChangePasswordA" );

    if ( pwdChangePassword != NULL )
    {
        (*pwdChangePassword)( "SCRSAVE", hwnd, 0, 0 );
    }

    FreeLibrary (hinstMPRDLL);
}

void doConfig( HWND hwnd )
{
    if (RegisterDialogClasses (g_hinstance))
    {
        DialogBoxParam ( g_hinstance, MAKEINTRESOURCE(DLG_SCRNSAVECONFIGURE),
                         hwnd, (DLGPROC) ScreenSaverConfigureDialog, 0 );
    }
}

void doSaver( HWND hwndParent )
{
    HDC         hdc;
    MSG         msg;
    HWND        hwnd;
    WNDCLASS    cls;
    HANDLE      hOther;
    RECT        rcParent;
    int         nx, ny, cx, cy = 0;
    wchar_t*    pszWindowTitle = NULL;
    UINT        uExStyle, uStyle;

    cls.style           = CS_OWNDC | CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW;
    cls.lpfnWndProc     = (WNDPROC) RealScreenSaverProc;
    cls.cbClsExtra      = 0;
    cls.cbWndExtra      = 0;
    cls.hInstance       = g_hinstance;
    cls.hIcon           = LoadIcon (g_hinstance, MAKEINTRESOURCE(ID_APP));
    cls.hCursor         = NULL;
    cls.hbrBackground   = (HBRUSH) GetStockObject (BLACK_BRUSH);
    cls.lpszMenuName    = NULL;
    cls.lpszClassName   = g_szClassName;

    if (hwndParent != NULL)
    {
        pszWindowTitle = wcsdup( L"Preview" );
        GetClientRect( hwndParent, &rcParent );

        cx = rcParent.right;
        cy = rcParent.bottom;
        nx = 0;
        ny = 0;
        uExStyle = 0;
        uStyle = WS_CHILD | WS_VISIBLE | WS_CLIPCHILDREN;    
    }
    else
    {
        pszWindowTitle = wcsdup( L"Screen Saver" );

        hOther = FindWindow (g_szClassName, pszWindowTitle);
          
        if (hOther != NULL && IsWindow ((HWND) hOther))
        {
            SetForegroundWindow( (HWND)hOther );
            return;
        }

        hdc = GetDC (HWND_DESKTOP);
        GetClipBox (hdc, &rcParent);
        ReleaseDC (HWND_DESKTOP, hdc);

        cx = rcParent.right - rcParent.left;
        cy = rcParent.bottom - rcParent.top;
        nx = rcParent.left;
        ny = rcParent.top;
        uExStyle = WS_EX_TOPMOST;
        uStyle = WS_POPUP | WS_VISIBLE | WS_CLIPSIBLINGS | WS_CLIPCHILDREN;
    }

    if (!RegisterClass (&cls))
        return;

    if (g_fOnWin95)
    {
        loadPwdDLL ();
        uShellMessage = RegisterWindowMessage ( L"QueryCancelAutoPlay" );
    }

    // Disable  the Alt+Tab, Ctrl+Alt+Delete, and Windows keys.
    hogMachine (TRUE);
    
    hwnd = CreateWindowEx( uExStyle, g_szClassName, pszWindowTitle, uStyle,
                           nx, ny, cx, cy, hwndParent, NULL, g_hinstance, NULL );

    if ( hwnd )
    {
        if (g_scrmode != smPreview)
            SetForegroundWindow (hwnd);

        while (GetMessage (&msg, NULL, 0, 0))
        {
            TranslateMessage (&msg);
            DispatchMessage (&msg);
        }
    }

    // Enable the Alt+Tab, Ctrl+Alt+Delete, and Windows keys.
    hogMachine (FALSE);

    if (g_fOnWin95)
    {
        unloadPwdDLL ();
    }
}

void hogMachine(BOOL fDisable)
{
    UINT uOldValue;

    if (!g_fOnWin95)
        return;

    // Disable or enable CTRL-ALT-DELETE, ALT-TAB, and Windows keys when running
    // under the Windows 95/98/ME family.
    SystemParametersInfo (SPI_SCREENSAVERRUNNING, fDisable, &uOldValue, 0);
}

void loadPwdDLL()
{
    HKEY hkey;
    DWORD dwSize, dwVal;

    if ( RegOpenKey(HKEY_CURRENT_USER, L"Control Panel\\Desktop", &hkey ) )
        return;

    dwSize = sizeof(DWORD);
    
    if ( RegQueryValueEx( hkey, L"ScreenSaveUsePassword", NULL, NULL, (LPBYTE) &dwVal, &dwSize) )
    {
        RegCloseKey (hkey);
        return;
    }

    RegCloseKey (hkey);

    if (dwVal != 0)
    {
        g_hinstPwdDLL = LoadLibrary( L"password.cpl" );
        if (!g_hinstPwdDLL)
            return;

        g_verifyScreenSavePwd = (VERIFYSCREENSAVEPWD)GetProcAddress(g_hinstPwdDLL, "VerifyScreenSavePwd");

        if (g_verifyScreenSavePwd != NULL)
            return;

        unloadPwdDLL ();
    }
}

void unloadPwdDLL()
{
    if (!g_hinstPwdDLL)
        return;

    FreeLibrary (g_hinstPwdDLL);
    g_hinstPwdDLL = NULL;
    g_verifyScreenSavePwd = NULL;
}

LRESULT CALLBACK RealScreenSaverProc( HWND hwnd, int iMsg, WPARAM wparam, LPARAM lparam )
{
    switch (iMsg)
    {
        case WM_CONTEXTMENU:
        case WM_HELP:
            if (g_scrmode == smPreview)
            {
                HWND hwndParent = GetParent (hwnd);
                if (hwndParent == NULL || !IsWindow (hwndParent))
                    return 1;
                PostMessage (hwndParent, iMsg, (WPARAM) hwndParent, lparam);
                return 1;
            }
            break;

        case WM_CREATE:
          GetCursorPos (&g_ptMouse);
          if (g_scrmode != smPreview)
                SetCursor (NULL);
          break;

        case WM_DESTROY:
            PostQuitMessage (0);
    }

    return ScreenSaverProc (hwnd, iMsg, wparam, lparam);
}