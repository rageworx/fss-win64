# Fake-Screen-Saver for Windows64
* A fake screen saver to prevent going screen locked.

## requirements
* MinGW-W64

## Additional included
* screensaver library implement for MinGW-W64.
* don't need to link useless MinGW-W64 libscrnsave.a and libscrnsavw.a.

## How to build ?
* Just type `make`.
