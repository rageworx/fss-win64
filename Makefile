# Makefile for MinGW-W64 + M-SYS
# ----------------------------------------------------------------------
# Written by Raph.K.
#

GCC = gcc
GPP = g++
AR  = ar
WRC = windres

# Base PATH
BASE_PATH = .
LLIB_PATH = $(BASE_PATH)/lib
SRC_PATH  = $(BASE_PATH)/src

# TARGET settings
TARGET_PKG = fss-win64.scr
TARGET_DIR = ./bin
TARGET_OBJ = ./obj
LIBWIN32S  = -lwinmm -lgdi32 -lcomctl32 -lcomdlg32

# DEFINITIONS
DEFS += -DWIN32 -D_WIN32 -DUNICODE -D_UNICODE

# Compiler optiops 
COPTS += -mwindows
COPTS += -fomit-frame-pointer -fexpensive-optimizations -O3 -s

# CC FLAGS
CFLAGS += -std=c++11
CFLAGS += -I$(SRC_PATH)
CFLAGS += -I$(LLIB_PATH) 
CFLAGS += -Ires
CFLAGS += $(DEFS)
CFLAGS += $(COPTS)

# Windows Resource Flags
WFLGAS  = $(CFLAGS)

# LINK FLAG
LFLAGS += -L$(LLIB_PATH)
LFLAGS += -static
LFLAGS += $(LIBWIN32S)

# Sources
SRCS = $(wildcard $(SRC_PATH)/*.cpp)

# Windows resource
WRES = res/resource.rc

# Make object targets from SRCS.
OBJS = $(SRCS:$(SRC_PATH)/%.cpp=$(TARGET_OBJ)/%.o)
WROBJ = $(TARGET_OBJ)/resource.o

.PHONY: prepare clean

all: prepare clean continue

continue: $(TARGET_DIR)/$(TARGET_PKG)

prepare:
	@mkdir -p $(TARGET_DIR)
	@mkdir -p $(TARGET_OBJ)

clean:
	@echo "Cleaning built targets ..."
	@rm -rf $(TARGET_DIR)/$(TARGET_PKG).*
	@rm -rf $(TARGET_INC)/*.h
	@rm -rf $(TARGET_OBJ)/*.o

$(OBJS): $(TARGET_OBJ)/%.o: $(SRC_PATH)/%.cpp
	@echo "Building $@ ... "
	@$(GPP) $(CFLAGS) -c $< -o $@

$(WROBJ): $(WRES)
	@echo "Building windows resource ..."
	@$(WRC) -i $(WRES) $(WFLAGS) -o $@

$(TARGET_DIR)/$(TARGET_PKG): $(OBJS) $(WROBJ)
	@echo "Generating $@ ..."
	@$(GPP) $(OBJS) $(WROBJ) $(CFLAGS) $(LFLAGS) -o $@
	@echo "done."
